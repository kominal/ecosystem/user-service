import SessionDatabase from './models/session/session.database';
import { info, error } from '@kominal/lib-node-logging';

export async function cleaner() {
	info('Starting cleaning job...');
	try {
		let date = new Date();
		date.setDate(date.getDate() - 7);

		const result = await SessionDatabase.deleteMany({ lastActive: { $lt: date.getTime() } });
		if (result.deletedCount && result.deletedCount > 0) {
			info(`Removed ${result.deletedCount} dead sessions!`);
		}
	} catch (e) {
		error('There was an error cleaning dead sessions!');
	}
}
