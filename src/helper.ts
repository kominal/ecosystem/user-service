import { hash } from 'bcryptjs';
import { Document } from 'mongoose';
import UserDatabase from './models/user/user.database';
import { SALT } from './helper/environment';

export async function login(username: string, password: string): Promise<Document> {
	const usernameHash = await hash(username, SALT);
	const passwordHash = await hash(password, SALT);

	const user = await UserDatabase.findOne({ usernameHash, passwordHash });

	if (!user) {
		throw 'error.login.failed';
	}

	return user;
}
