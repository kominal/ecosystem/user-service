import { model, Schema, Types } from 'mongoose';

const SessionDatabase = model(
	'Session',
	new Schema(
		{
			userId: Types.ObjectId,
			keyHash: {
				type: String,
				unique: true,
			},
			device: Schema.Types.Mixed,
			created: Number,
			lastActive: Number,
			masterEncryptionKey: Schema.Types.Mixed,
		},
		{ minimize: false }
	)
);

export default SessionDatabase;
