import { model, Schema } from 'mongoose';

export const ProfileDatabase = model(
	'Profile',
	new Schema(
		{
			userId: Schema.Types.ObjectId,
			active: Boolean,
			displaynameHash: String,
			displayname: Schema.Types.Mixed,
			publicKey: Schema.Types.Mixed,
			avatar: Schema.Types.Mixed,
			keyDisplayname: Schema.Types.Mixed,
			keyMasterEncryptionKey: Schema.Types.Mixed,
			updatedAt: Number,
		},
		{ minimize: false }
	)
);
