import { model, Schema } from 'mongoose';

const UserDatabase = model(
	'User',
	new Schema(
		{
			usernameHash: {
				type: String,
				unique: true,
			},
			passwordHash: String,
			resetTokenHash: String,
			resetData: Schema.Types.Mixed,
			masterEncryptionKey: Schema.Types.Mixed,
			privateKey: Schema.Types.Mixed,
			publicKey: Schema.Types.Mixed,
		},
		{ minimize: false }
	)
);

export default UserDatabase;
