import Service from '@kominal/service-util/helper/service';
import register from './routes/register';
import login from './routes/login';
import refresh from './routes/refresh';
import sessions from './routes/sessions';
import users from './routes/users';
import remove from './routes/remove';
import logout from './routes/logout';
import changePassword from './routes/changePassword';
import { cleaner } from './cleaner';
import profiles from './routes/profiles';

const service = new Service({
	id: 'user-service',
	name: 'User Service',
	description: 'Manages and authenticates users.',
	jsonLimit: '16mb',
	routes: [register, login, refresh, sessions, users, remove, logout, changePassword, profiles],
	database: 'user-service',
	scheduler: [{ task: cleaner, squad: true, frequency: 3600 }],
	rabbitMQ: async (channel) => {
		await channel.assertExchange('ecosystem.clients.outbound', 'fanout', { durable: false });
	},
});
service.start();

export default service;
