import { verifyParameter } from '@kominal/service-util/helper/util';
import { hash } from 'bcryptjs';
import Router from '@kominal/service-util/helper/router';
import { login } from '../helper';
import SessionDatabase from '../models/session/session.database';
import { SALT } from '../helper/environment';

const router = new Router();

/**
 * Returns a list of active sessions.
 * @group Protected
 * @security JWT
 * @route GET /list
 * @consumes application/json
 * @produces application/json
 * @returns {void} 200 - OK
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.getAsUser('/list', async (req, res, userId) => {
	const sessions = (await SessionDatabase.find({ userId }))
		.map((session) => session.toJSON())
		.map((session) => {
			const { _id, device, created, lastActive } = session;
			return {
				_id,
				device,
				created,
				lastActive,
			};
		});
	res.status(200).send(sessions);
});

/**
 * Creates a new session for a user.
 * @group Public
 * @route POST /sessions
 * @consumes application/json
 * @produces application/json
 * @param {string} username.body.required - The username
 * @param {string} password.body.required - The password
 * @param {AESEncrypted} device.body.required - The encrypted device identifier
 * @param {AESEncrypted} masterEncryptionKey.body.required - The encrypted master encryption key
 * @param {AESEncrypted} privateKey.body.required - The private key encrypted with the master encryption key
 * @param {AESEncrypted} key.body.required - The hash of the session key
 * @returns {void} 200 - OK
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.postAsGuest('/sessions', async (req, res) => {
	const { username, password, device, masterEncryptionKey, key } = req.body;
	verifyParameter(username, password, device?.data, device?.iv, key);

	const user = await login(username, password);

	const keyHash = await hash(key, SALT);

	await SessionDatabase.create({
		userId: user._id,
		keyHash,
		device,
		created: Date.now(),
		lastActive: Date.now(),
		masterEncryptionKey,
	});

	res.status(200).send();
});

export default router.getExpressRouter();
