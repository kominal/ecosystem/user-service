import { verifyParameter } from '@kominal/service-util/helper/util';
import { hash } from 'bcryptjs';
import Router from '@kominal/service-util/helper/router';
import { ProfileDatabase } from '../models/profile/profile.database';
import UserDatabase from '../models/user/user.database';
import { SALT } from '../helper/environment';

const router = new Router();

/**
 * Creates a new user.
 * @group Public
 * @route POST /register
 * @consumes application/json
 * @produces application/json
 * @param {string} username.body.required - The username
 * @param {AESEncrypted} displayname.body.required - The displayname
 * @param {string} password.body.required - The password
 * @param {string} resetToken.body.required - The reset token
 * @param {AESEncrypted} resetData.body.required - The encrypted resetData
 * @param {AESEncrypted} masterEncryptionKey.body.required - The encrypted masterEncryptionKey
 * @param {AESEncrypted} privateKey.body.required - The encrypted privateKey
 * @param {AESEncrypted} publicKey.body.required - The encrypted publicKey
 * @param {AESEncrypted} encryptedDisplayname.body.required - The encrypted displayname
 * @returns {void} 200 - The user was created and can be used to log in
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.postAsGuest('/register', async (req, res) => {
	const {
		username,
		displaynameHash,
		password,
		resetToken,
		resetData,
		masterEncryptionKey,
		privateKey,
		publicKey,
		displayname,
		profilePublicKey,
		keyDisplayname,
		keyMasterEncryptionKey,
	} = req.body;
	verifyParameter(
		username,
		displaynameHash,
		password,
		resetToken,
		resetData?.data,
		resetData?.iv,
		masterEncryptionKey?.data,
		masterEncryptionKey?.iv,
		privateKey?.data,
		privateKey?.iv,
		publicKey?.data,
		publicKey?.iv
	);

	const profile = await ProfileDatabase.findOne({ displaynameHash, active: true });
	if (profile) {
		throw 'error.displayname.inuse';
	}

	const usernameHash = await hash(username, SALT);
	const passwordHash = await hash(password, SALT);
	const resetTokenHash = await hash(resetToken, SALT);

	if (await UserDatabase.findOne({ usernameHash })) {
		throw 'error.username.inuse';
	}

	const user = await UserDatabase.create({
		usernameHash,
		passwordHash,
		resetTokenHash,
		resetData,
		masterEncryptionKey,
		privateKey,
		publicKey,
	});

	await ProfileDatabase.create({
		userId: user._id,
		active: true,
		displayname,
		publicKey: profilePublicKey,
		displaynameHash,
		keyDisplayname,
		keyMasterEncryptionKey,
		updatedAt: Date.now(),
	});

	res.status(200).send();
});

export default router.getExpressRouter();
