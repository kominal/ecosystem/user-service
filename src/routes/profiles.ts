import Router from '@kominal/service-util/helper/router';
import { verifyParameter } from '@kominal/service-util/helper/util';
import service from '..';
import { Profile } from '@kominal/ecosystem-models/profile';
import { ProfileDatabase } from '../models/profile/profile.database';

const router = new Router();

/**
 * Get all active profiles
 * @group Protected
 * @security JWT
 * @route GET /
 * @consumes application/json
 * @produces application/json
 * @returns {Array} 200 - Array of ProfileEncrypted
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.getAsUser('/profile', async (req, res, userId) => {
	const profiles: Profile[] = [];
	for (const profile of await ProfileDatabase.find({ userId, active: true })) {
		const p = profile.toJSON();
		profiles.push({
			id: p._id,
			userId: p.userId,
			displayname: p.displayname,
			publicKey: p.publicKey,
			displaynameHash: p.displaynameHash,
			keyDisplayname: p.keyDisplayname,
			keyMasterEncryptionKey: p.keyMasterEncryptionKey,
			avatar: p.avatar,
			updatedAt: p.updatedAt,
		});
	}

	res.status(200).send(profiles);
});

/**
 * Get all active profiles
 * @group Protected
 * @security JWT
 * @route GET /displayname/{displaynameHash}
 * @consumes application/json
 * @produces application/json
 * @returns {Array} 200 - Array of ProfileEncrypted
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.getAsGuest('/profile/displayname/:displaynameHash', async (req, res) => {
	const displaynameHash = req.params.displaynameHash;
	verifyParameter(displaynameHash);

	const profile = await ProfileDatabase.findOne({ displaynameHash: decodeURIComponent(displaynameHash), active: true });

	if (!profile) {
		throw 'error.profile.notfound';
	}

	const p = profile.toJSON();
	const profileEncrypted: Profile = {
		id: p._id,
		userId: p.userId,
		displayname: p.displayname,
		publicKey: p.publicKey,
		displaynameHash: p.displaynameHash,
		keyDisplayname: p.keyDisplayname,
		avatar: p.avatar,
		updatedAt: p.updatedAt,
	};

	res.status(200).send(profileEncrypted);
});

/**
 * Get all active profiles
 * @group Protected
 * @security JWT
 * @route GET /{profileId}/{updatedAt}
 * @consumes application/json
 * @produces application/json
 * @returns {Array} 200 - Array of ProfileEncrypted
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.getAsUser('/profile/:profileId/:updatedAt*?', async (req, res) => {
	const { profileId, updatedAt } = req.params;
	verifyParameter(profileId);

	const profile = await ProfileDatabase.findById(profileId);

	if (!profile) {
		throw 'error.profile.notfound';
	}

	const p = profile.toJSON();

	if (updatedAt && p.updatedAt === Number(updatedAt)) {
		res.status(304).send();
		return;
	}

	const profileEncrypted: Profile = {
		id: p._id,
		userId: p.userId,
		displayname: p.displayname,
		publicKey: p.publicKey,
		displaynameHash: p.displaynameHash,
		keyDisplayname: p.keyDisplayname,
		keyMasterEncryptionKey: undefined,
		avatar: p.avatar,
		updatedAt: p.updatedAt,
	};

	res.status(200).send(profileEncrypted);
});

/**
 * Create a profile
 * @group Protected
 * @security JWT
 * @route POST /
 * @consumes application/json
 * @produces application/json
 * @param {string} displaynameHash.body.required - The hash of the displayname
 * @param {AESEncrypted} displayname.body.required - The displayname encrypted with the masterEncryptionKey
 * @returns {Array} 200 - Array of ProfileEncrypted
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.postAsUser('/profile', async (req, res, userId) => {
	const { displaynameHash, displayname, publicKey, keyDisplayname, keyMasterEncryptionKey } = req.body;
	verifyParameter(
		displaynameHash,
		displayname?.data,
		displayname?.iv,
		publicKey?.data,
		publicKey?.iv,
		keyDisplayname?.data,
		keyDisplayname?.iv,
		keyMasterEncryptionKey?.data,
		keyMasterEncryptionKey?.iv
	);

	if (await ProfileDatabase.findOne({ displaynameHash, active: true })) {
		throw 'error.displayname.inuse';
	}

	if ((await ProfileDatabase.countDocuments({ userId, active: true })) >= 3) {
		throw 'error.profiles.tomany';
	}

	const { _id } = await ProfileDatabase.create({
		userId,
		active: true,
		displayname,
		publicKey,
		displaynameHash,
		keyDisplayname,
		keyMasterEncryptionKey,
		updatedAt: Date.now(),
	});

	await service.getRMQClient().publish('ecosystem.clients.outbound', '', {
		headers: {
			userIds: [userId],
		},
		type: 'ecosystem.profile.created',
		content: {
			profileId: _id,
		},
	});

	res.status(200).send();
});

/**
 * Update a profile
 * @group Protected
 * @security JWT
 * @route PUT /{profileId}
 * @consumes application/json
 * @produces application/json
 * @param {string} displaynameHash.body.required - The hash of the displayname
 * @param {AESEncrypted} displayname.body.required - The displayname encrypted with the masterEncryptionKey
 * @returns {Array} 200 - Array of ProfileEncrypted
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.putAsUser('/profile/:profileId', async (req, res, userId) => {
	const { profileId } = req.params;
	const { avatar, displayname } = req.body;

	if (!(await ProfileDatabase.findOne({ userId, _id: profileId, active: true }))) {
		throw 'error.profile.notfound';
	}

	let update: any = {};

	if (displayname) {
		const { displaynameHash, displayname, keyDisplayname } = req.body;
		verifyParameter(displaynameHash, displayname?.data, displayname?.iv, keyDisplayname?.data, keyDisplayname?.iv);

		update = {
			displayname,
			displaynameHash,
			keyDisplayname,
		};
	}

	if (avatar) {
		verifyParameter(avatar?.data, avatar?.iv);
		update.avatar = avatar;
	}

	if (Object.keys(update).length > 0) {
		const updatedAt = Date.now();
		update.updatedAt = updatedAt;
		await ProfileDatabase.updateOne({ userId, _id: profileId }, update);

		await service.getRMQClient().publish('ecosystem.clients.outbound', '', {
			headers: {
				userIds: [userId],
			},
			type: 'ecosystem.profile.updated',
			content: {
				profileId,
			},
		});

		//TODO Inform services about changes
	}

	res.status(200).send();
});

/**
 * Delete a profile
 * @group Protected
 * @security JWT
 * @route POST /{profileId}
 * @consumes application/json
 * @produces application/json
 * @param {string} displaynameHash.body.required - The hash of the displayname
 * @param {AESEncrypted} displayname.body.required - The displayname encrypted with the masterEncryptionKey
 * @returns {Array} 200 - Array of ProfileEncrypted
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.deleteAsUser('/profile/:profileId', async (req, res, userId) => {
	const { profileId } = req.params;
	verifyParameter(profileId);

	if (!(await ProfileDatabase.findOne({ userId, _id: profileId, active: true }))) {
		throw 'error.profile.notfound';
	}

	if ((await ProfileDatabase.countDocuments({ userId, active: true })) === 1) {
		throw 'error.profile.last';
	}

	await ProfileDatabase.updateMany({ userId, _id: profileId }, { active: false, keyMasterEncryptionKey: undefined, avatar: undefined });

	await service.getRMQClient().publish('ecosystem.clients.outbound', '', {
		headers: {
			userIds: [userId],
		},
		type: 'ecosystem.profile.deleted',
		content: {
			profileId,
		},
	});

	res.status(200).send();
});

export default router.getExpressRouter();
