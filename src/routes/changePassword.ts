import { verifyParameter } from '@kominal/service-util/helper/util';
import { hash } from 'bcryptjs';
import Router from '@kominal/service-util/helper/router';
import UserDatabase from '../models/user/user.database';
import { SALT } from '../helper/environment';

const router = new Router();

/**
 * Validates the current password and changes the passwort to a new one.
 * @group Protected
 * @route POST /changePassword
 * @consumes application/json
 * @produces application/json
 * @param {string} password.body.required - The current password of the user
 * @param {string} newPassword.body.required - The new password
 * @param {string} resetToken.body.required - The reset token
 * @param {AESEncrypted} resetData.body.required - The encrypted resetData
 * @param {AESEncrypted} masterEncryptionKey.body.required - The encrypted masterEncryptionKey
 * @returns {void} 200 - The password was changed
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.postAsUser('/changePassword', async (req, res, userId) => {
	const { password, newPassword, resetToken, resetData, masterEncryptionKey } = req.body;
	verifyParameter(password, resetToken, resetData?.data, resetData?.iv, masterEncryptionKey?.data, masterEncryptionKey?.iv);

	const passwordHash = await hash(password, SALT);

	const user = await UserDatabase.findOne({ _id: userId, passwordHash });

	if (!user) {
		throw 'error.password.wrong';
	}

	const newPasswordHash = await hash(newPassword, SALT);
	const resetTokenHash = await hash(resetToken, SALT);

	await UserDatabase.updateOne(
		{ _id: userId },
		{
			passwordHash: newPasswordHash,
			resetTokenHash,
			resetData,
			masterEncryptionKey,
		}
	);

	res.status(200).send();
});

export default router.getExpressRouter();
