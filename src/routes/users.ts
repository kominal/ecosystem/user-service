import { verifyParameter } from '@kominal/service-util/helper/util';
import Router from '@kominal/service-util/helper/router';
import { hash } from 'bcryptjs';
import UserDatabase from '../models/user/user.database';
import SessionDatabase from '../models/session/session.database';
import { SALT } from '../helper/environment';

const router = new Router();

/**
 * Returns the encrypted public key of a specific user.
 * The public key is encrypted with the hash of the displayname of the user.
 * To verify that the requested user is eledgible to request the public key
 * the two times hashed displayname has to be provided to this endpoint.
 * @group Public
 * @route POST /users/publicKey
 * @consumes application/json
 * @produces application/json
 * @param {string} displayname.body.required - The two times hashed displayname
 * @returns {AESEncrypted} 200 - The public key encrypted with the hash of the displayname of the user
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.postAsGuest('/users/publicKey', async (req, res) => {
	const { displayname } = req.body;
	verifyParameter(displayname);

	const displaynameHash = await hash(displayname, SALT);

	const user = await UserDatabase.findOne({ displaynameHash });

	if (!user) {
		throw 'error.user.notfound';
	}

	res.status(200).send(user.get('publicKey'));
});

/**
 * Returns the id of the user.
 * To identify the user, the two times hashed displayname has to be provided.
 * @group Public
 * @route POST /users/id
 * @consumes application/json
 * @produces application/json
 * @param {string} displayname.body.required - The two times hashed displayname
 * @returns {string} 200 - The id of the user
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.postAsGuest('/users/id', async (req, res) => {
	const { displayname } = req.body;
	verifyParameter(displayname);

	const displaynameHash = await hash(displayname, SALT);

	const user = await SessionDatabase.findOne({ displaynameHash });

	if (!user) {
		throw 'error.user.notfound';
	}

	res.status(200).send(user._id);
});

export default router.getExpressRouter();
