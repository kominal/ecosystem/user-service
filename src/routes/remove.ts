import Router from '@kominal/service-util/helper/router';
import { hash } from 'bcryptjs';
import { verifyParameter } from '@kominal/service-util/helper/util';
import UserDatabase from '../models/user/user.database';
import SessionDatabase from '../models/session/session.database';
import { SALT } from '../helper/environment';

const router = new Router();

/**
 * Deletes an account.
 * @group Protected
 * @security JWT
 * @route POST /deleteAccount
 * @consumes application/json
 * @produces application/json
 * @param {string} password.body.required - The user password
 * @returns {void} 200 - The account was deleted.
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.postAsUser('/deleteAccount', async (req, res, userId) => {
	const { password } = req.body;
	verifyParameter(password);

	const passwordHash = await hash(password, SALT);

	const user = await UserDatabase.findOne({ _id: userId, passwordHash });

	if (!user) {
		throw 'error.password.invalid';
	}

	await UserDatabase.deleteOne({ _id: userId });
	await SessionDatabase.deleteOne({ userId });

	res.status(200).send();
});

/**
 * Removes a session.
 * @group Protected
 * @security JWT
 * @route DELETE /remove/{id}
 * @consumes application/json
 * @produces application/json
 * @param {string} id.path.required - The session id.
 * @returns {void} 200 - The session was deleted.
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.deleteAsUser('/remove/:id', async (req, res, userId) => {
	await SessionDatabase.deleteMany({ _id: req.params.id, userId });
	res.status(200).send();
});

export default router.getExpressRouter();
