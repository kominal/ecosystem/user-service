import { hash } from 'bcryptjs';
import Router from '@kominal/service-util/helper/router';
import SessionDatabase from '../models/session/session.database';
import { SALT } from '../helper/environment';

const router = new Router();

/**
 * Invalidates the provided session key.
 * @group Public
 * @route POST /logout
 * @consumes application/json
 * @produces application/json
 * @returns {void} 200 - Sessions key was either already invalid or was invalidated
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.postAsGuest('/logout', async (req, res) => {
	if (!req.headers['authorization'] || !req.headers['authorization'].split(' ')[1]) {
		res.status(200).send();
		return;
	}

	await SessionDatabase.deleteMany({ keyHash: await hash(req.headers['authorization'].split(' ')[1], SALT) });

	res.status(200).send();
});

export default router.getExpressRouter();
