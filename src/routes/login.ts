import { verifyParameter } from '@kominal/service-util/helper/util';
import Router from '@kominal/service-util/helper/router';
import { login } from '../helper';

const router = new Router();

/**
 * Validates the username and password.
 * @group Public
 * @route POST /login
 * @consumes application/json
 * @produces application/json
 * @param {string} username.body.required - The username of the user
 * @param {string} password.body.required - The password of the user
 * @returns {AESEncrypted} 200 - The information required for session creation
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.postAsGuest('/login', async (req, res) => {
	const { username, password } = req.body;
	verifyParameter(username, password);

	const user = await login(username, password);

	res.status(200).send(user.get('masterEncryptionKey'));
});

export default router.getExpressRouter();
