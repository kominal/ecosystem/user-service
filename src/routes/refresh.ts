import { sign } from 'jsonwebtoken';
import { hash } from 'bcryptjs';
import Router from '@kominal/service-util/helper/router';
import SessionDatabase from '../models/session/session.database';
import UserDatabase from '../models/user/user.database';
import { JWT_KEY, SALT } from '../helper/environment';

const router = new Router();

/**
 * Generates a new JWT based on the provided session key.
 * @group Protected
 * @route POST /refresh
 * @consumes application/json
 * @produces application/json
 * @returns {object} 200 - The JWT object
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 * @security REFRESH_TOKEN
 */
router.postAsGuest('/refresh', async (req, res) => {
	if (!req.headers['authorization'] || !req.headers['authorization'].split(' ')[1]) {
		throw 'error.session.invalid';
	}

	const keyHash = await hash(req.headers['authorization'].split(' ')[1], SALT);

	const session = await SessionDatabase.findOne({ keyHash });

	if (!session) {
		throw 'error.session.invalid';
	}

	const userId = session.get('userId');

	const user = await UserDatabase.findOne({ _id: userId });

	if (!user) {
		throw 'error.user.notfound';
	}

	await SessionDatabase.updateOne({ keyHash }, { lastActive: Date.now() });

	const jwtExpires = Date.now() + 60 * 10 * 1000;

	const jwt = sign(
		{
			userId,
			exp: jwtExpires,
		},
		JWT_KEY
	);

	res.status(200).send({
		userId,
		jwt,
		jwtExpires: jwtExpires,
		masterEncryptionKey: session.get('masterEncryptionKey'),
		privateKey: user.get('privateKey'),
		publicKey: user.get('publicKey'),
	});
});

export default router.getExpressRouter();
