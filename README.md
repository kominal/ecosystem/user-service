# User Service

The user service is used to store information about all users. This includes settings as well as login credentials.
Connect used 3 values during the login process, the username, the displayname and the password.
All values are hashed on the client so the plain value can never be found by the server.
The displayname is used to encrypt your public key using AES which is later used to securly share a group key with your communication partner.
The plain displayname is shared with your communication parter which is why it can not be used to log you in.
The username is never shared with anyone and can therfore be used to authenicate you.

## Documentation

Production: https://ecosystem.kominal.com/user-service/api-docs
Test: https://ecosystem-test.kominal.com/user-service/api-docs
